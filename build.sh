#!/bin/bash
mkdir bin
g++ -Wall -std=c++11 -g test_thread.cpp -o ./bin/test_thread -lpthread
chmod 755 ./bin/test_thread
