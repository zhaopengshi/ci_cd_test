#include<iostream>
#include<thread>
#include<unistd.h>
using namespace std;

int num=100;
void thread01()
{
	while(num>0)
	{
		cout<<num<<endl;
		num--;
		usleep(100);
	}
}
void thread02()
{
	while(num>0)
	{
		cout<<num<<endl;
		num--;
		usleep(20);	
	}
}
void thread03(int num)
{
	for(int i=0; i<num;i++)
	{
		cout<<"thread03 is working"<<endl;
		usleep(100);
	}
}
void thread04()
{
	for(int i=0;i<5;i++)
	{
		cout<<"thread04 is working"<<endl;
		usleep(200);	
	}
}
void thread05()
{
	while(num>0)
	{
		cout<<num<<endl;
		num--;
		usleep(1);	
	}
}

int main()
{
	thread task1(thread01);
	thread task2(thread02);
	thread task5(thread05);
	task1.detach();
	task2.detach();
	task5.detach();

	

	/*thread task03(thread03,10);
	thread task04(thread04);
	task03.join();
	task04.join();	
	task03.detach();
	task04.detach();*/
	for(int i=0;i < 5;i++)
	{
		cout<<"Main Thread working!"<<endl;
		sleep(20);
	}
}

